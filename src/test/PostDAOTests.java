import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class PostDAOTests {

    private JdbcTemplate mockJdbc;

    @BeforeEach
    void PreTestBehaviour() {
        mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);
    }

    @Test
    @DisplayName("setPost test 1")
    void ThreadListTest1() {
        Post oldPost = new Post("me", new Timestamp(0), "myForum", "myMessage", 0, 0, false);
        Post newPost = new Post("me", new Timestamp(0), "myForum", "myMessage", 0, 0, false);

        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        verify(mockJdbc).queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1));
        verifyNoMoreInteractions(mockJdbc);
    }

    @Test
    @DisplayName("setPost test 2")
    void ThreadListTest2() {
        Post oldPost = new Post("me", new Timestamp(0), "myForum", "myMessage", 0, 0, false);
        Post newPost = new Post("notMe", new Timestamp(0), "myForum", "myMessage", 0, 0, false);

        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("setPost test 3")
    void ThreadListTest3() {
        Post oldPost = new Post("me", new Timestamp(0), "myForum", "myMessage", 0, 0, false);
        Post newPost = new Post("me", new Timestamp(1), "myForum", "myMessage", 0, 0, false);

        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("setPost test 4")
    void ThreadListTest4() {
        Post oldPost = new Post("me", new Timestamp(0), "myForum", "myMessage", 0, 0, false);
        Post newPost = new Post("me", new Timestamp(0), "myForum", "notMyMessage", 0, 0, false);

        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("setPost test 5")
    void ThreadListTest5() {
        Post oldPost = new Post("me", new Timestamp(0), "myForum", "myMessage", 0, 0, false);
        Post newPost = new Post("notMe", new Timestamp(1), "myForum", "myMessage", 0, 0, false);

        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("setPost test 6")
    void ThreadListTest6() {
        Post oldPost = new Post("me", new Timestamp(0), "myForum", "myMessage", 0, 0, false);
        Post newPost = new Post("me", new Timestamp(1), "myForum", "notMyMessage", 0, 0, false);

        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("setPost test 7")
    void ThreadListTest7() {
        Post oldPost = new Post("me", new Timestamp(0), "myForum", "myMessage", 0, 0, false);
        Post newPost = new Post("notMe", new Timestamp(0), "myForum", "notMyMessage", 0, 0, false);

        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("setPost test 8")
    void ThreadListTest8() {
        Post oldPost = new Post("me", new Timestamp(0), "myForum", "myMessage", 0, 0, false);
        Post newPost = new Post("notMe", new Timestamp(1), "myForum", "notMyMessage", 0, 0, false);

        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

}
